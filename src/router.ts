import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '@/views/Home.vue';
import Genesis from '@/views/Genesis.vue';
import Renaswap from '@/views/Renaswap.vue';
import About from '@/views/About.vue';
import Stake from '@/views/Stake.vue';
import Reserve from '@/views/Reserve.vue';
import Rebalance from '@/views/Rebalance.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  { path: '/', name: 'home', component: Home },
  { path: '/genesis', name: 'genesis', component: Genesis },
  { path: '/renaswap', name: 'renaswap', component: Renaswap },
  { path: '/rebalance', name: 'rebalance', component: Rebalance },
  { path: '/about', name: 'about', component: About },
  { path: '/stake', name: 'stake', component: Stake },
  { path: '/reserve', name: 'reserve', component: Reserve }
];

const router = new VueRouter({
  routes
});

export default router;
